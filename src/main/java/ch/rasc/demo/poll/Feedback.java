package ch.rasc.demo.poll;

public class Feedback {
	Long cmtId;
	String commentTxt;

	public Long getCmtId() {
		return cmtId;
	}

	public void setCmtId(Long cmtId) {
		this.cmtId = cmtId;
	}

	public String getCommentTxt() {
		return commentTxt;
	}

	public void setCommentTxt(String commentTxt) {
		this.commentTxt = commentTxt;
	}

}
