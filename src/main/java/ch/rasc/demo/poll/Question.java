package ch.rasc.demo.poll;

import java.util.ArrayList;

public class Question {
	
	String name;
	Long qnId;
	String otp;
	ArrayList<Option> options;
	
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getQnId() {
		return qnId;
	}
	public void setQnId(Long qnId) {
		this.qnId = qnId;
	}
	public ArrayList<Option> getOptions() {
		return options;
	}
	public void setOptions(ArrayList<Option> options) {
		this.options = options;
	}
	

}
