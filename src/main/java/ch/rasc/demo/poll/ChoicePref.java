package ch.rasc.demo.poll;

public class ChoicePref {
	Long opId;
	public ChoicePref(Long opId, int priority) {
		super();
		this.opId = opId;
		this.priority = priority;
	}
	public Long getOpId() {
		return opId;
	}
	public void setOpId(Long opId) {
		this.opId = opId;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	int priority;

}
