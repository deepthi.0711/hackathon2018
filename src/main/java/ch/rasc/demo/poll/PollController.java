package ch.rasc.demo.poll;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletResponse;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestTemplate;

import ch.rasc.sse.eventbus.SseEventBus;

@Controller
public class PollController {

	private final SseEventBus eventBus;

	private final DB db;

	// private final ConcurrentMap<String, Long> pollMap;
	private final ConcurrentMap<Long, String> qstnNameqIdMap;
	private final ConcurrentMap<Long, String> optIdOptNameMap;
	private final ConcurrentMap<Long, Long> optIdQidMap;
	private final ConcurrentMap<Long, String> qnIdOtp;
	// private final ConcurrentMap<Long, String> optIdNameMap;
	private final ConcurrentMap<Long, Long> optIdValueMap;
	/*
	 * private final ConcurrentMap<Long, Long> cmtIdQidMap; private final
	 * ConcurrentMap<Long, String> cmtIDTextMap;
	 */

	private final ConcurrentMap<Long, String> cmtIdText;
	private final ConcurrentMap<Long, String> cmtIdSentiment;
	Random rand = new Random();

	private final static String[] oss = { "Windows", "macOS", "Linux", "Other" };

	PollController(SseEventBus eventBus) {
		this.eventBus = eventBus;

		this.db = DBMaker.fileDB("./counter1.db").transactionEnable().make();

		// this.pollMap = this.db.hashMap("polls", Serializer.STRING,
		// Serializer.LONG).createOrOpen();
		this.qstnNameqIdMap = this.db.hashMap("qstnNameqIdMap", Serializer.LONG, Serializer.STRING).createOrOpen();
		this.optIdOptNameMap = this.db.hashMap("optIdOptNameMap", Serializer.LONG, Serializer.STRING).createOrOpen();
		this.optIdQidMap = this.db.hashMap("optIdQidMap", Serializer.LONG, Serializer.LONG).createOrOpen();
		// this.optIdNameMap = this.db.hashMap("optIdNameMap", Serializer.LONG,
		// Serializer.STRING).createOrOpen();
		this.optIdValueMap = this.db.hashMap("optIdValueMap", Serializer.LONG, Serializer.LONG).createOrOpen();
		this.cmtIdSentiment = this.db.hashMap("cmtIdSentiment", Serializer.LONG, Serializer.STRING).createOrOpen();
		this.cmtIdText = this.db.hashMap("cmtIdText", Serializer.LONG, Serializer.STRING).createOrOpen();
		this.qnIdOtp = this.db.hashMap("qnIdOtp", Serializer.LONG, Serializer.STRING).createOrOpen();

		/*
		 * for (String os : oss) { this.pollMap.putIfAbsent(os, 0L); }
		 */
	}

	@PreDestroy
	public void close() {
		if (this.db != null) {
			this.db.close();
		}
	}

	@CrossOrigin
	@RequestMapping(value = {
			"/getQuestionList" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Question[]> getAllQuestionList(HttpServletResponse response) {
		// SseEmitter sseEmitter = this.eventBus.createSseEmitter("701",
		// SseEvent.DEFAULT_EVENT);
		Question[] questionList = getQuestionListFromDB();

		/*
		 * SseEvent.Builder builder = SseEvent.builder().data(questionList);
		 * this.eventBus.handleEvent(builder.build());
		 * 
		 * return sseEmitter;
		 */
		return new ResponseEntity<Question[]>(questionList, HttpStatus.ACCEPTED);

	}

	@CrossOrigin
	@RequestMapping(value = {
			"/getQuestion/{qnId}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Question> getQuestion(HttpServletResponse response, @PathVariable("qnId") Long qnId) {
		// SseEmitter sseEmitter = this.eventBus.createSseEmitter("117",
		// SseEvent.DEFAULT_EVENT);

		Question question = new Question();
		question.setQnId(qnId);
		question.setName(qstnNameqIdMap.get(qnId));
		ArrayList<Option> options = getOptions(qnId);
		// Option[] option = options.toArray(new Option[options.size()]);

		question.setOptions(options);

		/*
		 * SseEvent.Builder builder = SseEvent.builder().data(options);
		 * this.eventBus.handleEvent(builder.build()); return sseEmitter;
		 */
		return new ResponseEntity<Question>(question, HttpStatus.ACCEPTED);

	}

	@CrossOrigin
	@RequestMapping(value = {
			"/getOtp/{qnId}/{otp}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getOtp(HttpServletResponse response, @PathVariable("qnId") Long qnId,
			@PathVariable("otp") String otp) {
		boolean res = qnIdOtp.get(qnId).equalsIgnoreCase(otp);
		if (res) {
			return new ResponseEntity<String>("SUCCESS", HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<String>("FAILURE", HttpStatus.ACCEPTED);
		}

	}

	private ArrayList<Option> getOptions(Long qnId) {
		System.out.println("in getOptions....................");

		ArrayList<Option> options = new ArrayList<>();
		for (Entry<Long, Long> optIdQidMapEntry : optIdQidMap.entrySet()) {
			System.out.println("iterate: " + optIdQidMapEntry.getValue());
			System.out.println("trying to match");
			System.out.println(optIdQidMapEntry.getValue() + "and " + qnId);
			int res = optIdQidMapEntry.getValue().compareTo(qnId);
			System.out.println();
			if (res == 0) {
				System.out.println("match found");
				Option option = new Option();
				option.setOptId(optIdQidMapEntry.getKey());
				option.setOptionText(optIdOptNameMap.get(option.getOptId()));
				option.setOptionCount(optIdValueMap.get(option.getOptId()));
				option.setQnId(qnId);
				// System.out.println("options: " +
				// optIdNameMap.get(option.getOptId()));

				options.add(option);
			}
		}

		return options;

	}

	private Question[] getQuestionListFromDB() {
		ArrayList<Question> questionsList = new ArrayList<>();

		for (Entry<Long, String> entry : qstnNameqIdMap.entrySet()) {
			Question question = new Question();
			ArrayList<Option> options = new ArrayList<>();
			question.setName(entry.getValue());
			question.setQnId(entry.getKey());
			question.setOtp(qnIdOtp.get(entry.getKey()));
			System.out.println("question id " + entry.getKey() + "  " + entry.getValue());

			for (Entry<Long, Long> optIdQidMapEntry : optIdQidMap.entrySet()) {
				System.out.println("iterate: " + optIdQidMapEntry.getValue());
				System.out.println("trying to match");
				System.out.println(optIdQidMapEntry.getValue() + "and " + question.getQnId());
				int res = optIdQidMapEntry.getValue().compareTo(question.getQnId());
				System.out.println();
				if (res == 0) {
					System.out.println("match found");
					Option option = new Option();
					option.setOptId(optIdQidMapEntry.getKey());
					option.setOptionText(optIdOptNameMap.get(option.getOptId()));
					option.setOptionCount(optIdValueMap.get(option.getOptId()));
					option.setQnId(entry.getKey());
					options.add(option);
				}
			}
			if (options != null && options.size() != 0) {
				question.setOptions(options);
				// questions.add(question);
			}
			questionsList.add(question);

		}

		Question[] question = questionsList.toArray(new Question[questionsList.size()]);
		return question;
	}

	/*
	 * private Question[] getQuestionListFromDB() { ArrayList<Question>
	 * questions = new ArrayList<>();
	 * 
	 * for (Entry<Long, String> entry : qstnNameqIdMap.entrySet()) { Question
	 * question = new Question(); ArrayList<Option> options = new ArrayList<>();
	 * question.setName(entry.getValue()); question.setQnId(entry.getKey());
	 * question.setOtp(qnIdOtp.get(entry.getKey()));
	 * System.out.println("question id " + entry.getKey() + "  " +
	 * entry.getValue());
	 * 
	 * for (Entry<Long, Long> optIdQidMapEntry : optIdQidMap.entrySet()) {
	 * System.out.println("iterate: " + optIdQidMapEntry.getValue());
	 * System.out.println("trying to match");
	 * System.out.println(optIdQidMapEntry.getValue() + "and " +
	 * question.getQnId()); int res =
	 * optIdQidMapEntry.getValue().compareTo(question.getQnId());
	 * System.out.println(); if (res == 0) { System.out.println("match found");
	 * Option option = new Option(); option.setOptId(optIdQidMapEntry.getKey());
	 * option.setOptionText(optIdOptNameMap.get(option.getOptId()));
	 * option.setOptionCount(optIdValueMap.get(option.getOptId()));
	 * option.setQnId(entry.getKey()); // System.out.println("options: " + //
	 * optIdNameMap.get(option.getOptId())); options.add(option); }
	 * 
	 * } if (options != null && options.size() != 0) {
	 * question.setOptions(options); questions.add(question); }
	 * 
	 * }
	 * 
	 * Question[] question = questions.toArray(new Question[questions.size()]);
	 * return question; }
	 */

	/*
	 * @GetMapping("/register/{id}") public SseEmitter
	 * register(@PathVariable("id") String id, HttpServletResponse response) {
	 * response.setHeader("Cache-Control", "no-store"); SseEmitter sseEmitter =
	 * this.eventBus.createSseEmitter(id, SseEvent.DEFAULT_EVENT);
	 * 
	 * // send the initial data only to this client sendPollData(id);
	 * 
	 * return sseEmitter; }
	 */

	@CrossOrigin
	@RequestMapping(value = { "/vote" }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void vote(@RequestBody Option option) {
		Long qnId = option.getQnId();
		String optionText = option.getOptionText();
		for (Entry<Long, String> optIdOptNameMapEntry : optIdOptNameMap.entrySet()) {
			System.out.println("found text: " + optIdOptNameMapEntry.getValue());

			if (optIdOptNameMapEntry.getValue().equalsIgnoreCase(optionText)) {
				System.out.println("found matchinng text: " + optIdOptNameMapEntry.getValue());

				Long optid = optIdOptNameMapEntry.getKey();
				System.out.println("retrieved opt id" + optid);

				for (Entry<Long, Long> optIdQidMapEntry : optIdQidMap.entrySet()) {
					System.out.println("from qnMap..optid" + optIdQidMapEntry.getKey());
					int res = optIdQidMapEntry.getKey().compareTo(optid);
					System.out.println("matched..optId.." + optIdQidMapEntry.getKey());
					if (res == 0) {
						int res2 = optIdQidMapEntry.getValue().compareTo(qnId);
						if (res2 == 0) {
							Long count = optIdValueMap.get(optid);
							count = count + 1;
							optIdValueMap.put(optid, count);
							System.out.println("new count: " + count + "optId" + optid);
							this.db.commit();
						}

					}
				}

			}
		}

		/*
		 * SseEvent.Builder builder = SseEvent.builder().data(option);
		 * this.eventBus.handleEvent(builder.build());
		 */
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = {
			"/addFeedback" }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void addFeedback(@RequestBody Feedback feedback) {
		System.out.println("In addFeedback....");

		Long cId = (long) (rand.nextInt(60) + 1);
		this.cmtIdText.put(cId, feedback.getCommentTxt());
		// cmtIdText.put(cId, comment.getCommentTxt());
		String sentiment = getSentiment(feedback.getCommentTxt());
		System.out.println("Succes......");
		cmtIdSentiment.put(cId, sentiment);
		this.db.commit();
		/*
		 * SseEvent.Builder builder1 = SseEvent.builder().data(comment);
		 * this.eventBus.handleEvent(builder1.build());
		 */
	}

	@CrossOrigin
	@RequestMapping(value = { "/addQues" }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void addQuestion(@RequestBody Question ques) {

		Long qid = (long) (rand.nextInt(50) + 1);

		this.qstnNameqIdMap.put(qid, ques.getName());
		if (ques.getOptions() != null && ques.getOptions().size() != 0) {
			for (Option option : ques.getOptions()) {
				Long oid = (long) (rand.nextInt(50) + 1);
				optIdQidMap.put(oid, qid);
				optIdOptNameMap.put(oid, option.getOptionText());
				optIdValueMap.put(oid, 0L);
				qnIdOtp.put(qid, ques.getOtp());
			}
			this.db.commit();
		}

		/*
		 * SseEvent.Builder builder = SseEvent.builder().data(ques);
		 * this.eventBus.handleEvent(builder.build());
		 */
	}

	private String getSentiment(String comment) {
		System.out.println("Trying to access amazon.....");
		final String uri = "https://98iwhunz47.execute-api.us-east-1.amazonaws.com/dev/ping?comment=" + comment;

		RestTemplate restTemplate = new RestTemplate();
		System.out.println("Generated rest template....");
		String result = restTemplate.getForObject(uri, String.class);
		System.out.println("result is..." + result);
		String[] tokens = result.split(",");
		String sentiment = tokens[0].substring(tokens[0].indexOf(':') + 1).trim();
		System.out.println(sentiment);
		return sentiment;
	}

	/*
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = { "/addComment" }, method = RequestMethod.POST,
	 * consumes = MediaType.APPLICATION_JSON_VALUE)
	 * 
	 * @ResponseStatus(code = HttpStatus.NO_CONTENT) public void
	 * addComment(@RequestBody Comment comment) {
	 * 
	 * Long cId = (long) (rand.nextInt(50) + 1); cmtIdText.put(cId,
	 * comment.getCommentTxt()); String sentiment =
	 * getSentiment(comment.getCommentTxt()); cmtIdSentiment.put(cId,
	 * sentiment); this.db.commit(); SseEvent.Builder builder1 =
	 * SseEvent.builder().data(comment);
	 * this.eventBus.handleEvent(builder1.build()); }
	 */

	@CrossOrigin
	@RequestMapping(value = { "/getComment" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Feedback[]> getComment(HttpServletResponse response) {
		HashMap<String, Object> cmtIdSentiment = new HashMap<>();
		Feedback[] commentList = getCommentListFromDB();
		cmtIdSentiment.put("comment1", commentList);
		cmtIdSentiment.put("comment2", "hello");
		/*
		 * SseEmitter sseEmitter = this.eventBus.createSseEmitter("9896",
		 * SseEvent.DEFAULT_EVENT); SseEvent.Builder builder1 =
		 * SseEvent.builder().data(commentList);
		 * this.eventBus.handleEvent(builder1.build()); return sseEmitter;
		 */
		return new ResponseEntity<Feedback[]>(commentList, HttpStatus.ACCEPTED);

	}

	@CrossOrigin
	@RequestMapping(value = {
			"/getSentimentCount" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Sentiment[]> getSentimentCount(HttpServletResponse response) {

		String[] sentimentList = getSentimentFromDB();
		int pos = 0;
		int neg = 0;
		int mix = 0;
		int neu = 0;
		for (String sentiment : sentimentList) {
			if (sentiment.equalsIgnoreCase("POSITIVE")) {
				pos++;
			}
			if (sentiment.equalsIgnoreCase("NEGATIVE")) {
				neg++;
			}
			if (sentiment.equalsIgnoreCase("NEUTRAL")) {
				neu++;
			}
			if (sentiment.equalsIgnoreCase("MIXED")) {
				mix++;
			}
		}
		Sentiment s1 = new Sentiment("POSITIVE", pos);
		Sentiment s2 = new Sentiment("NEGATIVE", neg);
		Sentiment s3 = new Sentiment("NEUTRAL", neu);
		Sentiment s4 = new Sentiment("MIXED", mix);
		ArrayList<Sentiment> results = new ArrayList<>();
		results.add(s1);
		results.add(s2);
		results.add(s3);
		results.add(s4);
		Sentiment[] sentiment = results.toArray(new Sentiment[results.size()]);
		return new ResponseEntity<Sentiment[]>(sentiment, HttpStatus.ACCEPTED);

		/*
		 * SseEmitter sseEmitter = this.eventBus.createSseEmitter("8988",
		 * SseEvent.DEFAULT_EVENT); SseEvent.Builder builder =
		 * SseEvent.builder().data(sentiment);
		 * this.eventBus.handleEvent(builder.build()); return sseEmitter;
		 */

	}

	private Feedback[] getCommentListFromDB() {
		ArrayList<Feedback> commentList = new ArrayList<>();
		for (Entry<Long, String> entry : cmtIdText.entrySet()) {
			Feedback c = new Feedback();
			c.setCmtId(entry.getKey());
			c.setCommentTxt(entry.getValue());
			// c.setSentiment(cmtIdSentiment.get(entry.getKey()));
			commentList.add(c);
		}
		Feedback[] comment = commentList.toArray(new Feedback[commentList.size()]);
		return comment;
	}

	private String[] getSentimentFromDB() {
		ArrayList<String> commentList = new ArrayList<>();
		for (Entry<Long, String> entry : cmtIdText.entrySet()) {
			commentList.add((cmtIdSentiment.get(entry.getKey())));
		}
		String[] comment = commentList.toArray(new String[commentList.size()]);
		return comment;
	}

	/*
	 * for (Entry<Long, Long> entry : cmtIdQidMap.entrySet()) { QuestionComment
	 * questionCmt = new QuestionComment(); ArrayList<Comment> commentList = new
	 * ArrayList<>(); questionCmt.setQnId(entry.getValue());
	 * System.out.println("question id" + entry.getValue());
	 * System.out.println("size of map" + cmtIDTextMap.size());
	 * 
	 * for (Entry<Long, String> cmtIDTextMapEntry : cmtIDTextMap.entrySet()) {
	 * int res = cmtIDTextMapEntry.getKey().compareTo(entry.getKey());
	 * System.out.println("key" + cmtIDTextMapEntry.getKey() + " value" +
	 * entry.getKey()); if (res == 0) { System.out.println("match found");
	 * Comment comment = new Comment();
	 * comment.setCmtId(cmtIDTextMapEntry.getKey());
	 * comment.setCommentTxt(cmtIDTextMapEntry.getValue());
	 * commentList.add(comment); } } questionCmt.setCommentList(commentList);
	 * questionCmtList.add(questionCmt);
	 */

	/*
	 * private void sendPollData(String clientId) { StringBuilder sb = new
	 * StringBuilder(10);
	 * 
	 * for (int i = 0; i < oss.length; i++) {
	 * sb.append(this.pollMap.get(oss[i])); if (i < oss.length - 1) {
	 * sb.append(','); } } // "{"a":22,"b":27,"c":5,"d":45}" data:0,0,5,0
	 * StringBuilder s = new StringBuilder(10);
	 * s.append("{\"a\":22,\"b\":27,\"c\":5,\"d\":45}"); SseEvent.Builder
	 * builder = SseEvent.builder().data(s.toString()); if (clientId != null) {
	 * builder.addClientId(clientId); }
	 * this.eventBus.handleEvent(builder.build()); }
	 */

	/*
	 * @PostMapping("/poll")
	 * 
	 * @ResponseStatus(code = HttpStatus.NO_CONTENT) public void
	 * poll(@RequestBody String os) { this.pollMap.merge(os, 1L, (oldValue, one)
	 * -> oldValue + one); this.db.commit(); sendPollData(null); }
	 */

}