package ch.rasc.demo.poll;

public class MyForm {
	
	String text;
	String[] options;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String[] getOptions() {
		return options;
	}
	public void setOptions(String[] options) {
		this.options = options;
	}

}
