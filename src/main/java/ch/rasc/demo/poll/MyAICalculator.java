package ch.rasc.demo.poll;

import java.util.ArrayList;

public class MyAICalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<ChoicePref> choice = getSampleData();
		int c1 = 0,c2=0,c3=0;
		//vote calculations
		for(ChoicePref c: choice){
			int res1 = c.getOpId().compareTo(1L);
			if(res1==0){
				c1++;
			}
			int res2 = c.getOpId().compareTo(1L);
			if(res2==0){
				c2++;
			}
			int res3 = c.getOpId().compareTo(1L);
			if(res3==0){
				c3++;
			}
		}
		
		System.out.println("A"+c1);
		System.out.println("B"+c2);
		System.out.println("C"+c3);
	}
	
	

	private static ArrayList<ChoicePref> getSampleData() {
		ArrayList<ChoicePref> choice = new ArrayList<>();
		choice.add(new ChoicePref(1L,1));
		choice.add(new ChoicePref(2L,2));
		choice.add(new ChoicePref(3L,3));
		
		choice.add(new ChoicePref(1L,3));
		choice.add(new ChoicePref(2L,2));
		choice.add(new ChoicePref(3L,1));
		
		choice.add(new ChoicePref(1L,2));
		choice.add(new ChoicePref(2L,3));
		choice.add(new ChoicePref(3L,1));
		
		choice.add(new ChoicePref(1L,1));
		choice.add(new ChoicePref(2L,2));
		choice.add(new ChoicePref(3L,3));
		
		choice.add(new ChoicePref(1L,2));
		choice.add(new ChoicePref(2L,1));
		choice.add(new ChoicePref(3L,3));
		
		choice.add(new ChoicePref(1L,1));
		choice.add(new ChoicePref(2L,3));
		choice.add(new ChoicePref(3L,2));
		return choice;
	}

}
