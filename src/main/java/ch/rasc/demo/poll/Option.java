package ch.rasc.demo.poll;

public class Option {
	String optionText;
	Long qnId;
	Long optionCount;
	Long optId;
	public String getOptionText() {
		return optionText;
	}
	public void setOptionText(String optionText) {
		this.optionText = optionText;
	}
	public Long getQnId() {
		return qnId;
	}
	public void setQnId(Long qnId) {
		this.qnId = qnId;
	}
	public Long getOptionCount() {
		return optionCount;
	}
	public void setOptionCount(Long optionCount) {
		this.optionCount = optionCount;
	}
	public Long getOptId() {
		return optId;
	}
	public void setOptId(Long optId) {
		this.optId = optId;
	}
	
}
