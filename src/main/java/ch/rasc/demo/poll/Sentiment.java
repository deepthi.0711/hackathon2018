package ch.rasc.demo.poll;

public class Sentiment {
	String sentiment;
	int count;

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public int getCount() {
		return count;
	}

	public Sentiment(String sentiment, int count) {
		super();
		this.sentiment = sentiment;
		this.count = count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
