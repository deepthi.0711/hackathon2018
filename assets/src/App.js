import React, {Component} from 'react';
import { Container } from 'mdbreact';
import './App.css';
import './index.css';
import Routes from "./components/Routes";

class App extends Component {
  render() {
    return (
      <Container>
      <div className="flexible-content">
          <Routes/>
      </div>
      </Container>
    );
  }
}

export default App;
