import React from 'react'
import {Button, Col, Container, Input, Row, Modal, ModalBody, ModalHeader, ModalFooter} from 'mdbreact';
import ChartSection from "./sections/ChartSection";
import axios from "axios";
import Alert from "react-s-alert";
import {API_URL} from "./../../js/config"

class ViewQuestionPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      radio: '',
      question: {},
      display: false,
      modal: false,
      otp: ""
    };
    this.onClick = this.onClick.bind(this);
    this.onSubmitVote = this.onSubmitVote.bind(this);
    this.getQuestion = this.getQuestion.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.onOTPChange =  this.onOTPChange.bind(this);
  }

  toggleModal() {
    this.setState({
      modal: !this.state.modal
    });
  }

  componentDidMount() {
    if(this.props.load) {
      this.getQuestion();
    } else {
      this.setState({
        question : this.props.question,
        display : true
      })
    }
  }

  componentWillReceiveProps(props) {
    this.setState({
      question : props.question,
      display : true
    });
  }

  onClick(nr) {
    this.setState({
      radio: nr
    });
  };

  onSubmitVote() {
    if (this.state.otp) {
      this.setState({
        modal : false
      });
      axios({
        method: 'get',
        url: API_URL + "getOtp/" + this.state.question.qnId + "/" + this.state.otp,
      }).then(function (res) {
        if (res.data === "SUCCESS") {
          let vote = {
            qnId: this.state.question.qnId,
            optionText: this.state.radio
          };
          axios({
            method: 'post',
            url: API_URL + "vote",
            data: {...vote}
          }).then(function (res) {
            console.log("Success : ", res);
            Alert.success("Thank you for your feedback");
            this.props.reload();
            window.location.reload();
          }.bind(this)).catch(function (err) {
            Alert.error("Sorry, failed to get feedback. Please try again!");
            console.log("Feedback Catch : ", err);
          });
        }
      }.bind(this)).catch(function (err) {
        Alert.error("Sorry, failed to get otp. Please try again!");
        console.log("Feedback Catch : ", err);
      });
    }
  };

  getQuestion() {
    axios({
      method: 'get',
      url: API_URL + "getQuestion/" + this.props.qnId,
    }).then(function (res) {
      console.log("Success : ", res);
      let state = this.state;
      state.question = res.data;
      state.display = true;
      this.setState({...state});
      Alert.success("Thank you for your feedback");
    }.bind(this)).catch(function (err) {
      Alert.error("Sorry, failed to get feedback. Please try again!");
      console.log("Feedback Catch : ", err);
    });
  }

  onOTPChange(e) {
    e.preventDefault();
    let state = this.state;
    state.otp = e.target.value;
    this.setState({...state});
  }

  render() {
    if (this.state.display) {
      return (
        <Container>
          <Row>
            <Col md="7">
              <div>
                <p className="h5 text-left mb-4">Question</p>
                <div className="grey-text">
                  <p>{this.state.question.name}</p>
                </div>
                <div className="grey-text">
                  <Row>
                    <Col>
                      <Input gap
                             onClick={() => this.onClick(this.state.question.options[0].optionText)}
                             checked={this.state.radio === this.state.question.options[0].optionText ? true : false}
                             label={this.state.question.options[0].optionText}
                             type="radio"
                             id="radio1"
                      />
                    </Col>
                    <Col>
                      <Input
                        onClick={() => this.onClick(this.state.question.options[1].optionText)}
                        checked={this.state.radio === this.state.question.options[1].optionText ? true : false}
                        label={this.state.question.options[1].optionText}
                        type="radio"
                        id="radio2"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Input
                        onClick={() => this.onClick(this.state.question.options[2].optionText)}
                        checked={this.state.radio === this.state.question.options[2].optionText ? true : false}
                        label={this.state.question.options[2].optionText}
                        type="radio"
                        id="radio3"
                      />
                    </Col>
                    <Col>
                      <Input
                        onClick={() => this.onClick(this.state.question.options[3].optionText)}
                        checked={this.state.radio === this.state.question.options[3].optionText ? true : false}
                        label={this.state.question.options[3].optionText}
                        type="radio"
                        id="radio4"
                      />
                    </Col>
                  </Row>
                </div>
                <div className="text-left mt-5">
                  <button onClick={this.toggleModal} className="btn btn-primary">Vote</button>
                </div>
              </div>
            </Col>
            <Col md="5" className="mt-md-5 mt-0">
              <ChartSection options={this.state.question.options}/>
            </Col>
          </Row>
          <Modal isOpen={this.state.modal} toggle={this.toggleModal}>
            <ModalHeader toggle={this.toggleModal}>
              <div className="grey-text">
                <Input onChange={this.onOTPChange} label="Enter OTP here" group type="text" validate error="wrong" success="right"/>
              </div>
            </ModalHeader>
            <ModalFooter>
              <button color="secondary" className="btn btn-secondary" onClick={this.toggleModal}>Close</button>{' '}
              <button color="primary" className="btn btn-primary" onClick={this.onSubmitVote}>Verify</button>
            </ModalFooter>
          </Modal>
        </Container>
      );
    } else {
      return <div></div>;
    }

  }
}

export default ViewQuestionPage;
