import React from 'react'
import axios from "axios";
import Alert from "react-s-alert";
import ViewQuestionPage from "./ViewQuestionPage";
import {API_URL} from "./../../js/config"

class ViewQuestionList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      questions: []
    };

    this.getQuestionList = this.getQuestionList.bind(this);
  }

  componentDidMount() {
    this.getQuestionList();
  }

  getQuestionList() {
    axios({
      method: 'get',
      url: API_URL + "getQuestionList"
    }).then(function (res) {
      console.log("Success : ", res);
      let state = this.state;
      state.questions = res.data;
      this.setState({...state});
    }.bind(this)).catch(function (err) {
      Alert.error("Sorry, failed to load questions. Please try again!");
      console.log("Feedback Catch : ", err);
    });
  }

  render() {
    let {questions} = this.state;
    return (
      <div>
        {
          questions.map((question, index) => {
            return <ViewQuestionPage load={false} question={question} key={index} reload={this.getQuestionList.bind(this)}/>
          })
        }
      </div>
    )
  }
}

export default ViewQuestionList;
