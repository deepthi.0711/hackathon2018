import React from 'react';
import {Button, Col, Container, Fa, Input, Row} from 'mdbreact';
import CommentStream from './sections/CommentStream';
import axios from "axios";
import {API_URL} from "./../../js/config"

class AddFeedbackPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      commentTxt: ""
    };
    this.onChange = this.onChange.bind(this);
    this.sendFeedback = this.sendFeedback.bind(this);
  }

  onChange(e) {
    e.preventDefault();
    let state = this.state;
    state.commentTxt = e.target.value;
    this.setState({...state});
  }

  sendFeedback(e) {
    e.preventDefault();
    axios({
      method: 'post',
      url: API_URL + "addFeedback",
      data: {...this.state}
    }).then(function (res) {
      console.log("Success : ", res);
      window.location.reload();
    }).catch(function (err) {
      console.log("Catch : ", err);
    });
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
            <form>
              <p className="h5 text-center mb-4">Feedback</p>
              <div className="grey-text">
                <Input onChange={this.onChange} value={this.state.commentTxt} type="textarea" rows="2"
                       label="Your Feedback" icon="pencil"/>
              </div>
              <div className="text-center">
                <button outline color="secondary" onClick={this.sendFeedback}>Send <Fa icon="paper-plane-o"
                                                                                       className="ml-1"/></button>
              </div>
            </form>
          </Col>
        </Row>
        <Row className="mt-5">
          <CommentStream/>
        </Row>
      </Container>
    );
  }
};

export default AddFeedbackPage;
