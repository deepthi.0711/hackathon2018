import React from 'react'
import { Container, Row, Col, Input, Button, Modal, ModalFooter, ModalHeader } from 'mdbreact';
import axios from "axios";
import {API_URL} from "./../../js/config"

class AddQuestionPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name : '',
      options : [{optionText:""},{optionText:""},{optionText:""},{optionText:""}],
      modal: false,
      otp: ""
    };
    this.onQuestionChange = this.onQuestionChange.bind(this);
    this.onChange = this.onChange.bind(this);
    this.addQuestion = this.addQuestion.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.onOTPSet =  this.onOTPSet.bind(this);
  }

  toggleModal() {
    this.setState({
      modal: !this.state.modal
    });
  }

  onOTPSet(e) {
    e.preventDefault();
    let state = this.state;
    state.otp = e.target.value;
    this.setState({...state});
  }

  addQuestion(e) {
    e.preventDefault();
    this.setState({
      modal: !this.state.modal
    });
    if (this.state.otp) {
      axios({
        method: 'post',
        url: API_URL + "addQues",
        data: {...this.state}
      }).then(function (res) {
        console.log("Success : ", res);
        window.location.reload();
      }).catch(function (err) {
        console.log("Catch : ", err);
      });
    }
  }

  onChange(e) {
    e.preventDefault();
    console.log(e.target.id, e.target.value);
    let {options} = this.state
    options[e.target.id].optionText = e.target.value;
    this.setState({options : options});
    console.log(this.state);
  }

  onQuestionChange(e) {
    e.preventDefault();
    console.log("##", this.state);
    console.log("###", e);
    console.log("####", e.target);
    let state = this.state;
    state.name = e.target.value;
    this.setState({...state});
    console.log(this.state);
  }

  render () {
    console.log(this.state);
    return (
      <Container>
        <Row>
          <Col className="text-center">
            <div>
              <p className="h5 text-center mb-4">Question</p>
              <div className="grey-text">
                <Input onChange={this.onQuestionChange} label="Type your question" group type="text" validate error="wrong" success="right"/>
              </div>
              <div className="grey-text">
                <Row>
                  <Col md="6">
                    <Input onChange={this.onChange} label="Type your option A" group type="text" validate error="wrong" success="right" value={this.state.options[0].optionText} id="0"/>
                  </Col>
                  <Col md="6">
                    <Input onChange={this.onChange} label="Type your option B" group type="text" validate error="wrong" success="right" value={this.state.options[1].optionText} id="1"/>
                  </Col>
                </Row>
                <Row>
                  <Col md="6">
                    <Input onChange={this.onChange} label="Type your option C" group type="text" validate error="wrong" success="right" value={this.state.options[2].optionText} id="2"/>
                  </Col>
                  <Col md="6">
                    <Input onChange={this.onChange} label="Type your option D" group type="text" validate error="wrong" success="right" value={this.state.options[3].optionText} id="3"/>
                  </Col>
                </Row>
              </div>
              <div className="text-center">
                <button className="btn btn-primary" onClick={this.toggleModal}>Post</button>
              </div>
            </div>
          </Col>
        </Row>
        <Modal isOpen={this.state.modal} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>
            <div className="grey-text">
              <Input onChange={this.onOTPSet} label="Set OTP here" group type="text" validate error="wrong" success="right"/>
            </div>
          </ModalHeader>
          <ModalFooter>
            <button color="secondary" className="btn btn-secondary" onClick={this.toggleModal}>Close</button>{' '}
            <button color="primary" className="btn btn-primary" onClick={this.addQuestion}>Set</button>
          </ModalFooter>
        </Modal>
      </Container>
    );
  }
}

export default AddQuestionPage;
