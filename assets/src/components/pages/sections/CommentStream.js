import React, {Component} from 'react';
import {Col, Container, Row} from 'mdbreact';
import './style.css'
import axios from "axios";
import Alert from "react-s-alert";
import {API_URL} from "./../../../js/config"

class CommentStream extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: []
    };
    this.getComments = this.getComments.bind(this);
  }

  componentDidMount() {
    this.getComments();
  }

  getComments() {
    axios({
      method: 'get',
      url: API_URL + "getComment"
    }).then(function (res) {
      console.log("Success : ", res);
      let state = this.state;
      state.comments = res.data;
      this.setState({...state});
    }.bind(this)).catch(function (err) {
      Alert.error("Sorry, failed to load comments. Please try again!");
      console.log("Feedback Catch : ", err);
    });
  }

  render() {
    return (
      <Container>
        <Row>
          <div className="mdb-feed">
            {
              this.state.comments.map((comment, index) => {
                return (<div className="news" key={index}>
                  <div className="label">
                  </div>
                  <div className="excerpt">
                    <div className="brief">
                      <a className="name">Anonymous</a> commented
                      <div className="date">few minutes ago</div>
                    </div>
                    <div className="added-text">{comment.commentTxt}</div>
                  </div>
                </div>)
              })
            }
          </div>
        </Row>
      </Container>
    );
  }
}

export default CommentStream;
