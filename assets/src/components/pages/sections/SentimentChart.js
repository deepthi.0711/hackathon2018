import React, {Component} from 'react';
import {Badge, Card, CardBody, CardHeader, Col, Fa, ListGroup, ListGroupItem, Row} from 'mdbreact';
import {Bar} from 'react-chartjs-2';

class SentimentChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: []
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      options: nextProps.options
    });
  }

  componentDidMount() {
    this.setState({
      options: this.props.options
    });
  }

  render() {
    const {options} = this.state;
    const dataBar = {
      labels: options.map((option) => {
        return option.sentiment
      }),
      datasets: [{
        label: '# of Votes',
        data: options.map((option) => {
          return option.count
        }),
        backgroundColor: ['rgba(75, 192, 192, 0.2)', "rgba(255, 99, 132, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(255, 159, 64, 0.2)"],
        hoverBackgroundColor: ['rgba(75, 192, 192, 0.2)', "rgba(255, 99, 132, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(255, 159, 64, 0.2)"],
        borderColor: ['rgba(75, 192, 192, 1)', "rgba(255, 99, 132, 1)", "rgba(54, 162, 235, 1)", "rgba(255, 159, 64, 1)"],
        borderWidth: 1
      }]
    };
    const barOptions = {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    };
    return (
      <Card className="mb-4">
        <CardHeader>Sentiment Analyzer</CardHeader>
        <CardBody>
          <Bar data={dataBar} options={barOptions} height={300} options={{responsive: true}}/>
        </CardBody>
      </Card>
    )
  }
}

export default SentimentChart;