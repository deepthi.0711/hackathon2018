import React, {Component} from 'react';
import {Badge, Card, CardBody, CardHeader, Col, Fa, ListGroup, ListGroupItem, Row} from 'mdbreact';
import {Pie} from 'react-chartjs-2';

class ChartSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.setState({
      options: this.props.options
    });
  }

  render() {
    const {options} = this.state;
    const dataPie = {
      labels: options.map((option) => {
        return option.optionText
      }),
      datasets: [{
        data: options.map((option) => {
          return option.optionCount
        }),
        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
      }]
    };
    return (
      <Card className="mb-4">
        <CardHeader>Poll result</CardHeader>
        <CardBody>
          <Pie data={dataPie} height={150} options={{responsive: true}}/>
        </CardBody>
      </Card>
    )
  }
}

export default ChartSection;

