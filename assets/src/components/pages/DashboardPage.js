import React from 'react';
import {Row} from 'mdbreact';
import Iframe from 'react-iframe';
import SentimentChart from './sections/SentimentChart';
// import ViewQuestionPage from './ViewQuestionPage';
import CommentStream from './sections/CommentStream';
import axios from "axios";
import Alert from "react-s-alert";
import {API_URL} from "./../../js/config"

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sentimentReport : []
    };
    this.getSentimentReport =  this.getSentimentReport.bind(this);
  }

  componentDidMount() {
    this.getSentimentReport();
  }

  getSentimentReport () {
    axios({
      method: 'get',
      url: API_URL + "getSentimentCount"
    }).then(function (res) {
      console.log("Success : ", res);
      let state = this.state;
      state.sentimentReport = res.data;
      this.setState({...state});
    }.bind(this)).catch(function (err) {
      Alert.error("Sorry, failed to load questions. Please try again!");
      console.log("Feedback Catch : ", err);
    });
  }

  render () {
    return (
      <div className="px-3">
        <div className="row">
          <div className="col-sm-12 col-md-7 mb-sm-5 mb-0">
            <Iframe url="//www.slideshare.net/slideshow/embed_code/key/GXgCvCYkTsuuLu"
                    height="485px"
                    id="myId"
                    className=""
                    style={{"border": "1px solid #CCC", "borderWidth": "1px", "marginBottom": "5px", "maxWidth": "100%"}}
                    display="initial"
                    position="relative"
                    allowFullScreen/>
          </div>
          <div className="col-sm-12 col-md-5">
            <SentimentChart options={this.state.sentimentReport}/>
          </div>
        </div>
        {/*<div className="row mt-5">*/}
        {/*<ViewQuestionPage load={true} qnId={12}/>*/}
        {/*</div>*/}
        <div className="row mt-5">
          <CommentStream/>
        </div>
      </div>
    );
  }
}
export default DashboardPage;
