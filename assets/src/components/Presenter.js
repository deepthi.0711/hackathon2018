import React from "react";
import TopNavigationPresenter from "./topNavigationPresenter";
import { Route } from 'react-router-dom';
import AddQuestionPage from './pages/AddQuestionPage';
import DashboardPage from './pages/DashboardPage';
import ViewQuestionList from './pages/ViewQuestionList';

export default class Presenter extends React.Component {
  render () {
    return (
      <div>
        <TopNavigationPresenter />
        <Route path='/presenter/dashboard' component={DashboardPage} />
        <Route path='/presenter/add-question' component={AddQuestionPage} />
        <Route path='/presenter/view-question' component={ViewQuestionList} />
      </div>
    );
  }
}
