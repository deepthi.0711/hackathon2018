import React from "react";
import {withRouter, Link} from 'react-router-dom';
import {Button, Container} from "../App";
import './style.css';

const Main = () => (
  <div className="wrapper text-center">
    <h4>You are a </h4>
    <section>
      <Link to="/presenter/dashboard" color="primary" className="btn btn-primary">
        Presenter
      </Link>
      OR
      <Link to="/audience/view-question" className="btn btn-primary">Audience</Link>
    </section>
  </div>
);

export default withRouter(Main);
