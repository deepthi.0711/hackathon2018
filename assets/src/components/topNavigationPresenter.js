import React, { Component } from 'react';
import { Navbar, NavbarBrand, NavbarNav, NavItem, NavbarToggler, Collapse, Container } from 'mdbreact';
import { NavLink } from 'react-router-dom';

class TopNavigationPresenter extends Component {

  state = {
    collapseID: ''
  };

  toggleCollapse = collapseID => () => {
    this.setState(prevState => ({ collapseID: (prevState.collapseID !== collapseID ? collapseID : '') }));
  };

  render() {
    return (
      <Container className="mb-5">
        <Navbar color="light-blue lighten-4" light>
          <Container>
            <NavbarToggler onClick={this.toggleCollapse('navbarCollapse1')}/>
            <p className="pull-left">APAC Hackathon 2018</p>
            <Collapse id="navbarCollapse1" isOpen={this.state.collapseID} navbar>
              <NavbarNav left>
                <NavItem>
                  <NavLink to="/presenter/dashboard">Dashboard</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink to="/presenter/add-question">Add Question</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink to="/presenter/view-question">View Question</NavLink>
                </NavItem>
              </NavbarNav>
            </Collapse>
          </Container>
        </Navbar>
      </Container>
    );
  }
}

export default TopNavigationPresenter;
