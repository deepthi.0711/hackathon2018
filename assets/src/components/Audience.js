import TopNavigationAudience from "./topNavigationAudience";
import React from "react";
import { Route } from 'react-router-dom';
import ViewQuestionList from './pages/ViewQuestionList';
import AddFeedbackPage from "./pages/AddFeedbackPage";

export default class Audience extends React.Component {
   render () {
     return (
       <div>
         <TopNavigationAudience />
         <Route path='/audience/feedback' component={AddFeedbackPage} />
         <Route path='/audience/view-question' component={ViewQuestionList} />
       </div>
     );
   }
 }
