import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Presenter from "./Presenter";
import Audience from "./Audience";
import Main from "./Main";

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route path='/' exact component={Main} />
        <Route path='/presenter' component={Presenter} />
        <Route path='/audience' component={Audience} />
      </Switch>
    );
  }
}

export default Routes;
